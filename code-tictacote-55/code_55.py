import numpy as np
import pickle
import pandas as pd

BOARD_ROWS = 5
BOARD_COLS = 5

class State:
    # Menghitung berapakali terjadi kemenangan
    win_of_player_1=0
    win_of_player_2=0
    
    def __init__(self, p1, p2):
        self.board = np.zeros((BOARD_ROWS, BOARD_COLS))
        self.p1 = p1  # player1
        self.p2 = p2  # player2
        # jika kodisi selesai, dan memberikan kesimpulan (flag)
        self.isEnd = False
        # merekam setiap pergerakan pada papan tictac u/ pelajaran yg mendatang. [1 = p1, 0=kosong, -1 = p2]
        self.boardHash = None
        # init p1 plays first
        self.playerSymbol = 1

    # get unique hash of current board state
    def getHash(self):
        self.boardHash = str(self.board.reshape(BOARD_COLS*BOARD_ROWS))
        return self.boardHash
    
    # Mencari kemenangan
    def winner(self):
        
        # row
        for i in range(BOARD_ROWS):
            if sum(self.board[i, :]) == 5:
                self.isEnd = True
                return 1
            if sum(self.board[i, :]) == -5:
                self.isEnd = True
                return -1
        # col
        for i in range(BOARD_COLS):
            if sum(self.board[:, i]) == 5:
                self.isEnd = True
                return 1
            if sum(self.board[:, i]) == -5:
                self.isEnd = True
                return -1
            
        # diagonal
        diag_sum1 = sum([self.board[i, i] for i in range(BOARD_COLS)])
        diag_sum2 = sum([self.board[i, BOARD_COLS-i-1]
                        for i in range(BOARD_COLS)])
        diag_sum = max(diag_sum1, diag_sum2)
        if diag_sum == 5:
            self.isEnd = True
            return 1
        if diag_sum == -5:
            self.isEnd = True
            return -1

        # tie
        # no available positions
        if len(self.availablePositions()) == 0:
            self.isEnd = True
            return 0
        
        # not end
        self.isEnd = False
        return None

    def availablePositions(self):
        positions = []
        for i in range(BOARD_ROWS):
            for j in range(BOARD_COLS):
                if self.board[i, j] == 0:
                    positions.append((i, j))  # need to be tuple
        return positions

    def updateState(self, position):
        self.board[position] = self.playerSymbol
        # switch to another player
        self.playerSymbol = -1 if self.playerSymbol == 1 else 1

    # only when game ends
    def giveReward(self):
        result = self.winner()
        # backpropagate reward
        if result == 1:
            self.p1.feedReward(1)
            self.p2.feedReward(0)
        elif result == -1:
            self.p1.feedReward(0)
            self.p2.feedReward(1)
        else:
            self.p1.feedReward(0.1)
            self.p2.feedReward(0.5)

    # board reset
    def reset(self):
        self.board = np.zeros((BOARD_ROWS, BOARD_COLS))
        self.boardHash = None
        self.isEnd = False
        self.playerSymbol = 1
    
    # melakukan permainan
    def play(self, rounds=100, show_player_winner=False):
        for i in range(rounds):
            if i % 1000 == 0:
                # print("Rounds {}".format(i))
                pass
            
            while not self.isEnd:
                # Player 1
                positions = self.availablePositions()
                p1_action = self.p1.chooseAction(
                    positions, self.board, self.playerSymbol)
                # take action and upate board state
                self.updateState(p1_action)
                board_hash = self.getHash()
                self.p1.addState(board_hash)
                
                # check board status if it is end
                win = self.winner()
                
                # Pencatatan kemenangan
                if win == 1:
                    self.win_of_player_1 += 1
                    if show_player_winner:
                        print('palayer 1 WIN')
                        self.showBoard()
                if win == -1:
                    self.win_of_player_2 += 1
                    if show_player_winner:
                        print('palayer 2 WIN')
                        self.showBoard()
            
                if win is not None:
                    self.giveReward()
                    self.p1.reset()
                    self.p2.reset()
                    self.reset()
                    break
                else:                             
                    # Player 2
                    positions = self.availablePositions()
                    p2_action = self.p2.chooseAction(
                        positions, self.board, self.playerSymbol)
                    self.updateState(p2_action)
                    board_hash = self.getHash()
                    self.p2.addState(board_hash)
                    win = self.winner()
                    if win is not None:
                        self.giveReward()
                        self.p1.reset()
                        self.p2.reset()
                        self.reset()
                        break

    # play with human
    def play2(self, show_board= False):
        while not self.isEnd:
            # Player 1
            positions = self.availablePositions()
            p1_action = self.p1.chooseAction(
                positions, self.board, self.playerSymbol)
            # take action and upate board state
            self.updateState(p1_action)
            self.showBoard()
            # check board status if it is end
            win = self.winner()
            if win is not None:
                if win == 1:
                    print(self.p1.name, "wins!")
                else:
                    print("tie!")
                self.reset()
                break
            else:
                # Player 2
                positions = self.availablePositions()
                p2_action = self.p2.chooseAction(positions)

                self.updateState(p2_action)
                self.showBoard()
                win = self.winner()
                if win is not None:
                    if win == -1:
                        print(self.p2.name, "wins!")
                    else:
                        print("tie!")
                    self.reset()
                    break
    
    # Computer Vs Conmputer
    def play3(self):
        win_flag = ""
        while not self.isEnd:
            # Player 1
            positions = self.availablePositions()
            p1_action = self.p1.chooseAction(
                positions, self.board, self.playerSymbol)
            # take action and upate board state
            self.updateState(p1_action)
            # self.showBoard()
            # check board status if it is end
            win = self.winner()
            if win is not None:
                if win == 1:
                    # print(self.p1.name, "wins!")
                    win_flag = self.p1.name
                else:
                    # print("tie!")
                    pass
                self.reset()
                break
            else:
                # Player 2
                positions = self.availablePositions()
                p2_action = self.p2.chooseAction(positions, self.board, self.playerSymbol)
                # take action and upate board state
                self.updateState(p2_action)
                # self.showBoard()
                win = self.winner()
                if win is not None:
                    if win == -1:
                        # print(self.p2.name, "wins!")
                        win_flag = self.p2.name
                    else:
                        # print("tie!")
                        pass
                    self.reset()
                    break
        return win_flag
                
    # Memperlihatkan papan permainan
    def showBoard(self):
        # p1: x  p2: o
        for i in range(0, BOARD_ROWS):
            print('---------------------')
            out = '| '
            for j in range(0, BOARD_COLS):
                if self.board[i, j] == 1:
                    token = 'x'
                if self.board[i, j] == -1:
                    token = 'o'
                if self.board[i, j] == 0:
                    token = ' '
                out += token + ' | '
            print(out)
        print('---------------------')


class Player:
    def __init__(self, name, learning_rate=0.2, exp_rate=0.3):
        self.name = name
        self.states = []  # record all positions taken
        # state
        self.lr = learning_rate # kecepatan algoritma belajar => lr (learning rate), 0-1 : besar (cepat tetapi tidak teliti), kecil (lambat tapi teliti)
        # Policy
        self.exp_rate = exp_rate
        self.decay_gamma = 0.9
        # value
        self.states_value = {}  # state -> value

    def getHash(self, board):
        boardHash = str(board.reshape(BOARD_COLS*BOARD_ROWS))
        return boardHash

    def chooseAction(self, positions, current_board, symbol):
        if np.random.uniform(0, 1) <= self.exp_rate:
            # take random action
            idx = np.random.choice(len(positions))
            action = positions[idx]
        else:
            value_max = -999
            for p in positions:
                next_board = current_board.copy()
                next_board[p] = symbol
                next_boardHash = self.getHash(next_board)
                value = 0 if self.states_value.get(next_boardHash) is None else self.states_value.get(next_boardHash)
                # print("value", value)
                if value >= value_max:
                    value_max = value
                    action = p
        # print("{} takes action {}".format(self.name, action))
        return action

    # append a hash state
    def addState(self, state):
        self.states.append(state)

    # at the end of game, backpropagate and update states value
    def feedReward(self, reward):
        for st in reversed(self.states):
            if self.states_value.get(st) is None:
                self.states_value[st] = 0
            self.states_value[st] += self.lr*(self.decay_gamma*reward - self.states_value[st])
            reward = self.states_value[st]

    def reset(self):
        self.states = []
    
    # Menyimpan aturan 
    def savePolicy(self):
        fw = open('policy_' + str(self.name), 'wb')
        pickle.dump(self.states_value, fw)
        fw.close()
        
    # Mengambil kembali aturan dari permainan sebelumnya.
    def loadPolicy(self, file):
        fr = open(file,'rb')
        self.states_value = pickle.load(fr)
        fr.close()


class HumanPlayer:
    def __init__(self, name):
        self.name = name 
    
    def chooseAction(self, positions):
        while True:
            row = int(input("Input your action row:"))
            col = int(input("Input your action col:"))
            action = (row, col)
            if action in positions:
                return action
    
    # append a hash state
    def addState(self, state):
        pass
    
    # at the end of game, backpropagate and update states value
    def feedReward(self, reward):
        pass
            
    def reset(self):
        pass


# Project role
learnin_grate = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
jumlah_pertandingan = [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]
exploration_rate = [0, 0.2, 0.4, 0.6, 0.8, 1]

# Set Pandas dataframe
df = pd.DataFrame(columns=['learning_rate', 'exploration_rate', 'jumlah_pertandingan', 
                           'winTrain_P1', 'winTrain_P2', 'win_P1', 'win_P2'])

# Proses
for  l_rate in learnin_grate:
    for xp_rate in exploration_rate:
        for play in jumlah_pertandingan:        
            
            # Info
            print(f"Progress .... LR:{l_rate} X-Pr:{xp_rate} Play:{play},")

            # ----------- Lakukan proses latihan
            # Inisialisasi pemain
            p1 = Player(name="P1", learning_rate=l_rate, exp_rate=xp_rate)
            p2 = Player(name="P2", learning_rate=l_rate, exp_rate=xp_rate)
            st = State(p1, p2)
            # Melakukan training 
            st.play(play)
            winTrain_p1 = st.win_of_player_1
            winTrain_p2 = st.win_of_player_2
            # Save pollicy
            p1.savePolicy()
            p2.savePolicy()
            # Reset pencatatan latihan sebelumnya
            st.win_of_player_1=0
            st.win_of_player_2=0
            
            # ----------- Lakukan pertandingan komputer VS Computer
            # Set Pemain baru
            new_p1_name = "computer1"
            new_p2_name = "computer2"
            
            # Gunakan dataset sebelumnya, percobaan ini
            # Dataset yang digunakan adalah milik player1
            new_p1 = Player(new_p1_name)
            new_p1.loadPolicy("policy_p1")
            new_p2 = Player(new_p2_name)
            new_p2.loadPolicy("policy_p1")
            
            # Set state
            new_st = State(new_p1, new_p2)
            win_p1 = 0
            win_p2 = 0
            
            # Lakukan pertandingan, sebanyak jumlah pertandingan sebelumnya
            for i in range(play):
                # Start permainan
                menang = new_st.play3()
                # Cek siapa yang menang
                if menang == new_p1_name:
                    win_p1 = win_p1 +1 # Player1 WIN
                if menang == new_p2_name:
                    win_p2 = win_p2 +1 # Player2 WIN
            
            # Add to dataframe
            df = df._append({'learning_rate': l_rate, 'exploration_rate': xp_rate, 'jumlah_pertandingan': play,
                             'winTrain_P1':winTrain_p1, 'winTrain_P2':winTrain_p2, 'win_P1':win_p1, 'win_P2':win_p2}, ignore_index=True)
            # Info
            print(f"Done Progress For LR:{l_rate} X-Pr:{xp_rate} Play:{play},")

# Menetapkan primary key (Saving..)
df.set_index('learning_rate', inplace=True)
df.to_csv(f'hasil {jumlah_pertandingan[0]} - {jumlah_pertandingan[1]}.csv')
print('DONE !')